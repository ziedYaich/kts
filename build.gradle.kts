plugins {
    application

    id("com.palantir.git-version") version "0.12.3"
}

application {
    mainClassName = "samples.HelloWorld"

}

apply(plugin = "com.palantir.git-version")
version = (extensions.extraProperties.get("gitVersion") as? groovy.lang.Closure<*>)?.call() ?: "dirty"
val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra

java {
    sourceCompatibility = JavaVersion.VERSION_1_7
    targetCompatibility = JavaVersion.VERSION_1_7
}

dependencies {
    testCompile("junit:junit:4.12")
}

repositories {
    mavenCentral()
    jcenter()
}

tasks.printVersion {
    val details = versionDetails()
    val additional = mapOf<String, Any>(
            "gitHash" to details.gitHash,
            "gitHashFull" to details.gitHashFull,
            "lastTag" to details.lastTag,
            "commitDistance" to details.commitDistance,
            "isCleanTag" to details.isCleanTag
    )
    println("DETAILS: " + details)
    println("ADDITIONAL: " + additional)
}
